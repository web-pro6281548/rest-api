import { Module } from '@nestjs/common';
import { TemperatureController } from './temperature.controller';
import { TemperatureService } from './temperature.service';

@Module({
  imports: [TempertureModule],
  exports: [],
  controllers: [TemperatureController],
  providers: [TemperatureService],
})
export class TempertureModule {}
